# CS371p: Object-Oriented Programming Collatz Repo

* Name: Austin Yuan

* EID: ahy259

* GitLab ID: austinyuan00

* HackerRank ID: austinyuan00

* Git SHA: 19e43d689aa8ca67f9c569b616a60b030f911d70

* GitLab Pipelines: https://gitlab.com/austinyuan00/cs371p-collatz/-/pipelines

* Estimated completion time: 5

* Actual completion time: 6

* Comments: The code/logic used is very similar to the code I wrote for Collatz in CS373 SWE (Python).
